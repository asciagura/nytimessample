package com.andreasciagura.nytimessample.di

import com.andreasciagura.nytimessample.api.MostPopularApi
import com.andreasciagura.nytimessample.model.Article
import com.andreasciagura.nytimessample.repository.MostPopularRepository
import com.andreasciagura.nytimessample.viewmodel.ArticleViewModel
import com.andreasciagura.nytimessample.viewmodel.ArticlesListViewModel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val mainModule = module {
    single {
        OkHttpClient.Builder()
            .build()
    }

    single {
        Retrofit.Builder().client(get())
            .baseUrl("https://api.nytimes.com/svc/")
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single { get<Retrofit>().create(MostPopularApi::class.java) }

    factory { MostPopularRepository(get()) }

    viewModel { (article: Article) -> ArticleViewModel(article) }
    viewModel { ArticlesListViewModel(get()) }
}