package com.andreasciagura.nytimessample.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasciagura.nytimessample.databinding.ArticleItemLayoutBinding
import com.andreasciagura.nytimessample.model.Article
import com.bumptech.glide.Glide

class ArticlesAdapter(private val onClickListener: (ArticleViewHolder) -> Unit) :
    RecyclerView.Adapter<ArticlesAdapter.ArticleViewHolder>() {
    private var items: List<Article> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun updateSource(list: List<Article>?) {
        items = list ?: emptyList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        return ArticleViewHolder(
            ArticleItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.bindWith(items[position])
    }


    inner class ArticleViewHolder(private val binding: ArticleItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {



        init {
            binding.root.setOnClickListener {
                onClickListener(this)
            }
        }

        fun bindWith(article: Article) {
            binding.article = article
            article.getFirstMediaSmall().run {
                Glide.with(binding.thumbView)
                    .load(url)
                    .fitCenter()
                    .into(binding.thumbView)
            }
        }

        fun getArticle() = binding.article
    }
}