package com.andreasciagura.nytimessample.api

import com.andreasciagura.nytimessample.API_KEY
import com.andreasciagura.nytimessample.model.ApiResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface MostPopularApi {

    @GET("$ENDPOINT/viewed/1.json?api-key=$API_KEY")
    fun getViewedArticlesAsync(): Deferred<ApiResponse>

    companion object {
        private const val ENDPOINT = "mostpopular/v2"
    }
}