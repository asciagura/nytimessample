package com.andreasciagura.nytimessample.viewmodel

import androidx.lifecycle.ViewModel
import com.andreasciagura.nytimessample.model.Article

class ArticleViewModel(private val article: Article) : ViewModel() {

    fun getArticle(): Article = article

}