package com.andreasciagura.nytimessample.viewmodel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.andreasciagura.nytimessample.model.Article
import com.andreasciagura.nytimessample.repository.MostPopularRepository
import kotlinx.coroutines.launch

class ArticlesListViewModel(private val mostPopularRepository: MostPopularRepository) :
    ScopedViewModel() {

    private val mutableProgressRingVisibility = MutableLiveData<Int>()
    val progressRingVisibility: LiveData<Int>
        get() = mutableProgressRingVisibility

    private val mutableArticles = MutableLiveData<List<Article>>()
    val articles: LiveData<List<Article>>
        get() = mutableArticles

    fun getViewedArticles() {
        if (articles.value == null) {
            mutableProgressRingVisibility.postValue(View.VISIBLE)
            launch {
                val response = mostPopularRepository.getViewedArticles()
                mutableArticles.postValue(response.result)
                mutableProgressRingVisibility.postValue(View.GONE)
            }
        }
    }
}