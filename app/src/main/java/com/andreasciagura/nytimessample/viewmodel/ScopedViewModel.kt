package com.andreasciagura.nytimessample.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlin.coroutines.CoroutineContext

abstract class ScopedViewModel : ViewModel(), CoroutineScope {

    protected val job = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job


    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}