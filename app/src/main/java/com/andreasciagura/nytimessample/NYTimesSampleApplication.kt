package com.andreasciagura.nytimessample

import android.app.Application
import com.andreasciagura.nytimessample.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class NYTimesSampleApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@NYTimesSampleApplication)
            modules(mainModule)
            androidLogger()
        }

    }
}

const val API_KEY = "5ZG6qErlRqYTAGO0b2BwIgrLFVGIpYGq"