package com.andreasciagura.nytimessample.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.andreasciagura.nytimessample.databinding.FragmentArticleBinding
import com.andreasciagura.nytimessample.viewmodel.ArticleViewModel
import com.bumptech.glide.Glide
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

/**
 * A simple [Fragment] subclass.
 *
 */
class ArticleFragment : Fragment() {
    private lateinit var binding: FragmentArticleBinding
    private val articleViewModel: ArticleViewModel by viewModel {
        val args = arguments?.let {
            ArticleFragmentArgs.fromBundle(it)
        }
        parametersOf(args!!.article)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentArticleBinding.inflate(inflater, container, false).apply {
            article = articleViewModel.getArticle()
            articleViewModel.getArticle().getFirstMediaLarge().run {
                Glide.with(context!!)
                    .load(url)
                    .fitCenter()
                    .thumbnail(Glide.with(thumbView).load(articleViewModel.getArticle().getFirstMediaSmall().url).fitCenter())
                    .into(thumbView)
            }
        }
        return binding.root
    }
}