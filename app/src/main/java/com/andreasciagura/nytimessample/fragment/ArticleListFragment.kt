package com.andreasciagura.nytimessample.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.andreasciagura.nytimessample.adapter.ArticlesAdapter
import com.andreasciagura.nytimessample.databinding.FragmentArticleListBinding
import com.andreasciagura.nytimessample.model.Article
import com.andreasciagura.nytimessample.viewmodel.ArticlesListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class ArticleListFragment : Fragment() {

    private lateinit var binding: FragmentArticleListBinding
    private val articlesListViewModel: ArticlesListViewModel by viewModel()

    private val articleAdapter by lazy {
        ArticlesAdapter { holder ->
            holder.getArticle()?.run {
                findNavController().navigate(
                    ArticleListFragmentDirections.actionArticleListFragmentToArticleFragment(this)
                )
            }
        }
    }

    private val articlesObserver = Observer<List<Article>> { list ->
        articleAdapter.updateSource(list)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        articlesListViewModel.articles.observe(this, articlesObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentArticleListBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@ArticleListFragment
            articleRecyclerView.adapter = articleAdapter
            progressRingVisibility = articlesListViewModel.progressRingVisibility
        }
        articlesListViewModel.getViewedArticles()
        return binding.root
    }

}
