package com.andreasciagura.nytimessample.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Article(
    @SerializedName("abstract")
    val `abstract`: String,
    @SerializedName("adx_keywords")
    val adxKeywords: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("media")
    val media: List<Media>,
    @SerializedName("published_date")
    val publishedDate: String,
    @SerializedName("section")
    val section: String,
    @SerializedName("source")
    val source: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String
) : Parcelable {
    fun getFirstMediaSmall(): MediaMetadata =
        media.first().mediaMetadata.minWith(Comparator { media1, media2 ->
            media1.height.compareTo(
                media2.height
            )
        })!!

    fun getFirstMediaLarge(): MediaMetadata =
        media.first().mediaMetadata.maxWith(Comparator { media1, media2 ->
            media1.height.compareTo(
                media2.height
            )
        })!!

    constructor(source: Parcel) : this(
        source.readString()!!,
        source.readString()!!,
        source.readLong(),
        source.createTypedArrayList(Media.CREATOR)!!,
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readString()!!
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(abstract)
        writeString(adxKeywords)
        writeLong(id)
        writeTypedList(media)
        writeString(publishedDate)
        writeString(section)
        writeString(source)
        writeString(title)
        writeString(type)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Article> = object : Parcelable.Creator<Article> {
            override fun createFromParcel(source: Parcel): Article = Article(source)
            override fun newArray(size: Int): Array<Article?> = arrayOfNulls(size)
        }
    }
}