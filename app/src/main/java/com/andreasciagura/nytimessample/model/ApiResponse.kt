package com.andreasciagura.nytimessample.model

import com.google.gson.annotations.SerializedName

data class ApiResponse(
    @SerializedName("copyright")
    val copyright: String,
    @SerializedName("num_results")
    val numResults: Int,
    @SerializedName("results")
    val result: List<Article>,
    @SerializedName("status")
    val status: String
)