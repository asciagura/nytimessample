package com.andreasciagura.nytimessample.repository

import com.andreasciagura.nytimessample.api.MostPopularApi
import com.andreasciagura.nytimessample.model.ApiResponse


class MostPopularRepository(private val mostPopularApi: MostPopularApi) {

    suspend fun getViewedArticles(forceLoad: Boolean = false): ApiResponse {

        return mostPopularApi.getViewedArticlesAsync().await()
    }
}

